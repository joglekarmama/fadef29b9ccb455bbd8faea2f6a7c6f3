import React, { useState, useEffect } from "react";
import config from 'visual-config-exposer';
import Image1 from "./Images/ImageComp";
import Pointer from "./Pointer"
import Caption1 from "./Text/TextComp";
import "../styles.css";

export default function Main() {
  const [value, setValue] = useState(0);
  var state = [{...config.settings.cards}];
  var spreadArray = state[0]
  var image = Object.entries(spreadArray)[value][1].image 
  var text=Object.entries(spreadArray)[value][1].label
useEffect(() => {
  if(text==""){
    document.getElementsByTagName('h3')[0].style.visibility='hidden';
  }
  else if(text!=""){
    document.getElementsByTagName('h3')[0].style.visibility='visible';
  }
})

  function flipped(e) {
    
    
if(value < Object.entries(spreadArray).length-1) 
{
  
  if (!document.getElementsByClassName("App")[0].classList.contains("flip")) {
    document.getElementsByClassName("App")[0].classList.add("flip");
    
    
  
    setTimeout(function () {
      
      
      document.getElementsByClassName("Imageflip")[0].classList.add("extraFlip");
    }, 1000);
    setTimeout(function () {
      setValue(value + 1);
    }, 500)
    
  } 
  else {
    
    document.getElementsByClassName("App")[0].classList.remove("flip");
    
   
    setTimeout(function () {
     
      document.getElementsByClassName("Imageflip")[0].classList.remove("extraFlip");
    }, 1000);
    setTimeout(function () {
      setValue(value + 1);
    }, 500)
  }
 
} 
else if (value >= Object.entries(spreadArray).length-1) {
 
  if (
    document.getElementsByClassName("App")[0].classList.contains("flip")
  ) {
    document.getElementsByClassName("App")[0].classList.remove("flip");
    
    setTimeout(function () {
     
      document.getElementsByClassName("Imageflip")[0].classList.remove("extraFlip");
    }, 1000);
    setTimeout(function () {
      setValue(0);
    }, 500)
  } 
  
  else {
    
    document.getElementsByClassName("App")[0].classList.add("flip");
    setTimeout(function () {
      
      document.getElementsByClassName("Imageflip")[0].classList.add("extraFlip");
    }, 1000);
    setTimeout(function () {
      setValue(0);
    }, 500)
  }
}
    }
  return (
    <>
      <div onClick={flipped}  className="App">
        <div className='Imageflip'  >
         <Image1 image={image}/>
        
         </div>
         <h3><Caption1 text={text} /></h3>
         <Pointer />

      </div>
     
    </>
  );
}