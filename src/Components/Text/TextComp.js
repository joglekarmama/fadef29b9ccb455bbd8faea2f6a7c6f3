import React from 'react'
import config from 'visual-config-exposer';
export default function Caption1({text}) {

  return (
    <div>
      <h1>{text}</h1>
    </div>
  ) 
}